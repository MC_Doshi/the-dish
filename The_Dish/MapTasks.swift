//
//  MapTasks.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/5/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

// Resources
// http://www.appcoda.com/google-maps-api-tutorial/

import UIKit

class MapTasks: NSObject {
    
    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    let boundsBias = "&bounds=37.411218,-122.215005|37.461593,-121.124760"
    let googleMapsAPIKey = "&key=AIzaSyCkIlx4QcgF6kIBGu01RWcXsEMZ_RoBSA8"
    var lookupAddressResults: Dictionary<NSObject, AnyObject>!
    var fetchedFormattedAddress: String!
    var fetchedAddressLongitude: Double!
    var fetchedAddressLatitude: Double!
    
    override init() {
        super.init()
    }
    
    func geocodeAddress(address: String!, withCompletionHandler completionHandler: ((status: String, success: Bool) -> Void)) {
        
        if let lookupAddress = address {
            var geocodeURLString = baseURLGeocode + "address=" + lookupAddress + boundsBias
            
            geocodeURLString = geocodeURLString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())! //+ googleMapsAPIKey
            
            if let geocodeURL = NSURL(string: geocodeURLString) {
            
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let geocodingResultsData = NSData(contentsOfURL: geocodeURL)
                
                    let dictionary: Dictionary<NSObject, AnyObject>
                
                    do {
                        try dictionary = NSJSONSerialization.JSONObjectWithData(geocodingResultsData!, options: NSJSONReadingOptions.MutableContainers) as! Dictionary<NSObject, AnyObject>
                    
                        let status = dictionary["status"] as! String
                    
                        if status == "OK" {
                            let allResults = dictionary["results"] as! Array<Dictionary<NSObject, AnyObject>>
                            self.lookupAddressResults = allResults[0]
                        
                            // Keep the most important values.
                            self.fetchedFormattedAddress = self.lookupAddressResults["formatted_address"] as! String
                            let geometry = self.lookupAddressResults["geometry"] as! Dictionary<NSObject, AnyObject>
                            self.fetchedAddressLongitude = ((geometry["location"] as! Dictionary<NSObject, AnyObject>)["lng"] as! NSNumber).doubleValue
                            self.fetchedAddressLatitude = ((geometry["location"] as! Dictionary<NSObject, AnyObject>)["lat"] as! NSNumber).doubleValue
                        
                            completionHandler(status: status, success: true)
                        } else {
                        completionHandler(status: status, success: false)
                        }
                    } catch {
                        print(error)
                        completionHandler(status: "", success: false)
                    }
                })
            } 
        } else {
            completionHandler(status: "No valid address.", success: false)
        }
    }
    
    
}
