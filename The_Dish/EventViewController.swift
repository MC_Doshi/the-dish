//
//  EventViewController.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/3/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

import UIKit

class EventViewController: UIViewController {
    
    // Constants for this class
    private struct Constants {
        static let NotAvailable = "N/A"
        static let Food = "Food? "
        static let Yes = "Yes"
        static let No = "No"
        static let DefaultImage = "default_image.png"
    }

    var event: Event?
    
    @IBOutlet private weak var eventImageView: UIImageView!
    @IBOutlet private weak var eventTitleLabel: UILabel!
    @IBOutlet private weak var eventAttendanceLabel: UILabel!
    @IBOutlet private weak var eventFoodLabel: UILabel!
    @IBOutlet private weak var eventDateLabel: UILabel!
    @IBOutlet private weak var eventDescriptoinLabel: UILabel!
    @IBOutlet private weak var eventLocationLabel: UILabel!
    @IBOutlet weak var eventScrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let event = self.event {
            eventTitleLabel.text = event.name
            if event.isNumberOfPeopleAttendingAvailable {
                eventAttendanceLabel.text = String(event.numberOfPeopleAttending)
            } else {
                eventAttendanceLabel.text = Constants.NotAvailable
            }
            
            eventDateLabel.text = event.date
            eventLocationLabel.text = event.location
            
            if event.isFoodInfoAvailable {
                if event.food {
                    eventFoodLabel.text = Constants.Food + Constants.Yes
                } else {
                    eventFoodLabel.text = Constants.Food + Constants.No
                }
            } else {
                eventFoodLabel.text = Constants.Food + Constants.NotAvailable
            }
            
            if let picture = event.picture {
                eventImageView.image = picture
            } else if let eventImageLink = event.imageLink {
                if let eventPhotoURL = NSURL(string: eventImageLink) {
                    NSURLSession.sharedSession().dataTaskWithURL(eventPhotoURL, completionHandler: { (data,response, error) in
                        if error != nil {
                            print(error)
                            return
                        }
                        
                        if let imageData = data {
                            if let image = UIImage(data: imageData) {
                                dispatch_async(dispatch_get_main_queue()) {
                                    self.eventImageView.image = image
                                }
                            }
                        }
                    }).resume()
                } else {
                    eventImageView.image = UIImage(named: Constants.DefaultImage)
                }
            } else {
                eventImageView.image = UIImage(named: Constants.DefaultImage)
            }
            
            eventDescriptoinLabel.text = event.description
        }
    }
}
