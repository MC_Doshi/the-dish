//
//  AddEventTableViewController.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/3/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

// Resources
// http://www.ioscreator.com/tutorials/take-photo-tutorial-ios8-swift
// http://www.codingexplorer.com/choosing-images-with-uiimagepickercontroller-in-swift/

import UIKit
import GoogleMaps

class AddEventTableViewController: UITableViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet private weak var eventNameTextField: UITextField!
    @IBOutlet private weak var startDateLabel: UILabel!
    @IBOutlet private weak var startDatePicker: UIDatePicker!
    @IBOutlet private weak var endDateLabel: UILabel!
    @IBOutlet private weak var endDatePicker: UIDatePicker!
    @IBOutlet private weak var locationSearchView: UIView!
    @IBOutlet private weak var locationTextField: UITextField!
    @IBOutlet private weak var descriptionTextField: UITextView!
    @IBOutlet private weak var foodButton: CheckBox!
    @IBOutlet private weak var cameraButton: UIButton!
    @IBOutlet private weak var libraryButton: UIButton!
    @IBOutlet private weak var eventPhotoImageView: UIImageView!
    
    private var resultsViewController: GMSAutocompleteResultsViewController?
    private var searchController: UISearchController?
    
    private let placesClient = GMSPlacesClient.sharedClient()
    private var chosenLocation: GMSPlace?
    
    var event:Event!
    private var isStartDatePickerHidden = true
    private var isEndDatePickerHidden = true
    private var isFoodAvailable = false
    private var isPhotoTaken = false
    private lazy var imagePicker = UIImagePickerController()
    
    private struct Storyboard {
        static let AddNewEventSegue = "AddNewEventSegue"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeDatePickers()
        setStartDate()
        setEndDate()
        eventNameTextField.delegate = self
        locationTextField.delegate = self
        descriptionTextField.delegate = self
        
        /*let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddEventTableViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)*/
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        searchController?.searchBar.sizeToFit()
        locationSearchView.addSubview((searchController?.searchBar)!)
        searchController?.hidesNavigationBarDuringPresentation = true

        self.definesPresentationContext = true
    }
    
    deinit {
        searchController?.loadViewIfNeeded() // iOS 9
        //let _ = self.tblSearchController.view // iOS 8
    }
    
    private func initializeDatePickers() {
        let currentDate = startDatePicker.date
        startDatePicker.minimumDate = currentDate
        endDatePicker.minimumDate = currentDate
    }
    
    @IBAction func startDatePickerAction(sender: UIDatePicker) {
        setStartDate()
    }
    
    @IBAction func endDatePickerAction(sender: UIDatePicker) {
        setEndDate()
    }
    
    private func setStartDate() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy hh:mm a"
        let startDate = dateFormatter.stringFromDate(startDatePicker.date)
        startDateLabel.text = startDate
        if startDatePicker.date.compare(endDatePicker.date) == NSComparisonResult.OrderedDescending {
            endDatePicker.date = startDatePicker.date
            endDateLabel.text = startDate
        }
    }
    
    private func setEndDate() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy hh:mm a"
        let endDate = dateFormatter.stringFromDate(endDatePicker.date)
        endDateLabel.text = endDate
        if endDatePicker.date.compare(startDatePicker.date) == NSComparisonResult.OrderedAscending {
            startDatePicker.date = endDatePicker.date
            startDateLabel.text = endDate
        }
    }
    
    /*func dismissKeyboard() {
        //view.endEditing(true)
        UIApplication.sharedApplication().sendAction(
            #selector(UIResponder.resignFirstResponder), to:nil, from:nil, forEvent:nil)
    }*/
    
    @IBAction func takePhoto(sender: UIButton) {
        imagePicker.delegate = self
        imagePicker.sourceType = .Camera
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func choosePhotoFromLibrary(sender: UIButton) {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        if let eventPhoto = info[UIImagePickerControllerOriginalImage] as? UIImage {
            eventPhotoImageView.image = eventPhoto
            isPhotoTaken = true
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section == 0) {
            eventNameTextField.becomeFirstResponder()
        } else if(indexPath.section == 1 && indexPath.row == 0) {
            toggleStartDatePicker()
            if(!isEndDatePickerHidden){
                toggleEndDatePicker()
            }
        } else if(indexPath.section == 1 && indexPath.row == 2){
            toggleEndDatePicker()
            if(!isStartDatePickerHidden){
                toggleStartDatePicker()
            }
        } else if(indexPath.section == 2) {
            locationTextField.becomeFirstResponder()
        } else if(indexPath.section == 3 && indexPath.row == 0) {
            descriptionTextField.becomeFirstResponder()
        } else {
            if(!isStartDatePickerHidden){
                toggleStartDatePicker()
            }
            if(!isEndDatePickerHidden){
                toggleEndDatePicker()
            }
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(isStartDatePickerHidden && indexPath.section == 1 && indexPath.row == 1){
            return 0
        } else if(isEndDatePickerHidden && indexPath.section == 1 && indexPath.row == 3){
            return 0
        } else if(indexPath.section == 4 && indexPath.row == 1) {
            if isPhotoTaken {
                if let imageHeight = eventPhotoImageView.image?.size.height {
                    if let imageWidth = eventPhotoImageView.image?.size.width {
                        let scale = imageHeight / imageWidth
                        let cellWidth = tableView.frame.width
                        return cellWidth * scale
                    }
                }
            }
            return 0
        } else {
            return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
        }
    }
    
    private func toggleStartDatePicker(){
        isStartDatePickerHidden = !isStartDatePickerHidden
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    private func toggleEndDatePicker(){
        isEndDatePickerHidden = !isEndDatePickerHidden
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    // MARK: - Navigation
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == Storyboard.AddNewEventSegue {
            if(eventNameTextField.text == "") {
                let alert = UIAlertController(title: "Missing Event Name", message: "Please fill in the corresponding field.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return false
            }
            if(locationTextField.text == "") {
                let alert = UIAlertController(title: "Missing Location", message: "Please fill in the corresponding field.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return false
            }
        }
        
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == Storyboard.AddNewEventSegue {
            event = Event()
            if let eventName = eventNameTextField.text {
                event.name = eventName
            }
            if let date = startDateLabel.text {
                event.date = date
            }
            event.startDate = startDatePicker.date
            event.endDate = endDatePicker.date
            if let location = locationTextField.text {
                event.location = location
            }
            if let googleMapsLocation = chosenLocation {
                event.googleMapsLocation = googleMapsLocation
            }
            if let description = descriptionTextField.text {
                event!.description = description
            }
            if let picture = eventPhotoImageView.image {
                event.picture = picture
            }
            event!.isFoodInfoAvailable = true
            event!.food = foodButton.isChecked
            event.isNumberOfPeopleAttendingAvailable = true
            event!.numberOfPeopleAttending = 1
        }
    }
}

// Handle the user's selection.
extension AddEventTableViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWithPlace place: GMSPlace) {
        searchController?.active = false
        
        chosenLocation = place
        if let formattedAddress = place.formattedAddress {
            locationTextField.text = formattedAddress
        } else {
            locationTextField.text = place.name
        }
        
    }
    
    func resultsController(resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: NSError){
        print("Error: ", error.description)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictionsForResultsController(resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictionsForResultsController(resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
}