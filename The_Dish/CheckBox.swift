//
//  CheckBox.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/5/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

// Resources
// http://stackoverflow.com/questions/29117759/how-to-create-radio-buttons-and-checkbox-in-swift-ios

import UIKit

class CheckBox: UIButton {

    let checkedImage = UIImage(named: "checked_checkbox.png")! as UIImage
    let uncheckedImage = UIImage(named: "unchecked_checkbox.png")! as UIImage
    
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, forState: .Normal)
            } else {
                self.setImage(uncheckedImage, forState: .Normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(CheckBox.buttonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.isChecked = false
    }
    
    func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}