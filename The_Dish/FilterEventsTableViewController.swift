//
//  FilterEventsTableViewController.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/5/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

import UIKit

class FilterEventsTableViewController: UITableViewController {

    @IBOutlet private weak var isFilteredByTime: UISwitch!
    @IBOutlet private weak var isFilteredByLocation: UISwitch!
    @IBOutlet private weak var isFilteredByAttendance: UISwitch!
    @IBOutlet private weak var isFilteredByFood: UISwitch!

    private struct Storyboard {
        static let ApplyFiltersSegue = "ApplyFiltersSegue"
    }
    
    var filteredByTime: Bool!
    var filteredByLocation: Bool!
    var filteredByAttendance: Bool!
    var filteredByFood: Bool!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isFilteredByTime.on = filteredByTime
        isFilteredByLocation.on = filteredByLocation
        isFilteredByAttendance.on = filteredByAttendance
        isFilteredByFood.on = filteredByFood
    }
    
    // MARK: - Table view data source
    
    
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Storyboard.ApplyFiltersSegue {
            filteredByTime = isFilteredByTime.on
            filteredByLocation = isFilteredByLocation.on
            filteredByAttendance = isFilteredByAttendance.on
            filteredByFood = isFilteredByFood.on
        }
    }
    

}
