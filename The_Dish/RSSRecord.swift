//
//  RSSRecord.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/2/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

import Foundation

class RSSRecord {
    var title: String
    var link: String
    var date: String
    var location: String
    var description: String
    var pubDate: String
    var imageLink: String
    
    init(){
        self.title = ""
        self.link = ""
        self.date = ""
        self.location = ""
        self.description = ""
        self.pubDate = ""
        self.imageLink = ""
    }
}