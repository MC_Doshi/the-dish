//
//  XMLParser.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/2/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

// Resources
// http://www.appcoda.com/building-rss-reader-using-uisplitviewcontroller-uipopoverviewcontroller/
// https://github.com/PrashantMangukiya/SwiftRSSReader/blob/master/SwiftRSSReader/SwiftRSSReader/ListViewController.swift

import UIKit

@objc protocol XMLParserDelegate {
    func parsingWasFinished()
}

class XMLParser: NSObject, NSXMLParserDelegate {
    
    private struct XMLElementName {
        static let Item = "item"
        static let Title = "title"
        static let Description = "description"
        static let PubDate = "pubDate"
        static let Link = "link"
        static let Enclosure = "enclosure"
        static let URL = "url"
    }
    
    var rssRecordList = [RSSRecord]()
    private var rssRecord: RSSRecord?
    private var isTagFound = ["item": false , "title":false, "description":false, "pubDate": false , "link":false]
    private var tempDescription = ""
    var delegate: XMLParserDelegate?
    
    /* Removes HTML tags from description */
    private func parseDescription(tempDescription: String) -> String {
        var text = tempDescription
        
        // Saves date of event
        if let dateRangeStart = text.rangeOfString("Date: ") {
            if let dateRangeEnd = text.rangeOfString("</div>") {
                var date = text[dateRangeStart.endIndex..<dateRangeEnd.startIndex]
                date = date.stringByReplacingOccurrencesOfString(".", withString: "")
                
                rssRecord?.date = date
                text = text.substringFromIndex(dateRangeEnd.endIndex)
            }
        }
        
        // Saves location of event
        if let locationRangeStart = text.rangeOfString("Location: ") {
            if let locationRangeEnd = text.rangeOfString("</div>") {
                rssRecord?.location = text[locationRangeStart.endIndex..<locationRangeEnd.startIndex]
                text = text.substringFromIndex(locationRangeEnd.endIndex)
            }
        }
        
        // Removes HTML tags from event description
        if let descriptionRangeStart = text.rangeOfString("<div class=\"stanford-events-description\">") {
            if let descriptionRangeEnd = text.rangeOfString("</div>") {
                rssRecord?.description = text[descriptionRangeStart.endIndex..<descriptionRangeEnd.startIndex]
                text = text.substringWithRange(descriptionRangeStart.endIndex..<descriptionRangeEnd.startIndex)
                
                while(true) {
                    if let tagRangeStart = text.rangeOfString("<") {
                        if let tagRangeEnd = text.rangeOfString(">") {
                            let tagToRemove = text.substringWithRange(tagRangeStart.startIndex..<tagRangeEnd.endIndex)
                            text = text.stringByReplacingOccurrencesOfString(tagToRemove, withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                        }
                    } else {
                        break
                    }
                }
            }
        }
        return text
    }
    
    func startParsingWithContentsOfURL(rssURL: NSURL) {
        if let parser = NSXMLParser(contentsOfURL: rssURL) {
            parser.delegate = self
            parser.parse()
        }
    }
    
     /* Detection of an element */
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        switch elementName {
        case XMLElementName.Item:
            self.isTagFound[XMLElementName.Item] = true
            self.rssRecord = RSSRecord()
        case XMLElementName.Title:
            self.isTagFound[XMLElementName.Title] = true
        case XMLElementName.Description:
            self.isTagFound[XMLElementName.Description] = true
        case XMLElementName.Link:
            self.isTagFound[XMLElementName.Link] = true
        case XMLElementName.PubDate:
            self.isTagFound[XMLElementName.PubDate] = true
        case XMLElementName.Enclosure:
            if let imageLink = attributeDict[XMLElementName.URL] {
                self.rssRecord?.imageLink += imageLink
            }
        default:
            break
        }
    }
    
    /* Handles characters received by some element */
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if isTagFound[XMLElementName.Title] == true {
            self.rssRecord?.title += string
        } else if isTagFound[XMLElementName.Description] == true {
            tempDescription += string
        } else if isTagFound[XMLElementName.Link] == true {
            self.rssRecord?.link += string
        } else if isTagFound[XMLElementName.PubDate] == true {
            self.rssRecord?.pubDate += string
        }
    }
    
    /* End detection of an element */
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        switch elementName {
        case XMLElementName.Item:
            self.isTagFound[XMLElementName.Item] = false
            self.rssRecordList.append(self.rssRecord!)
        case XMLElementName.Title:
            self.isTagFound[XMLElementName.Title] = false
        case XMLElementName.Description:
            self.isTagFound[XMLElementName.Description] = false
            self.rssRecord?.description = parseDescription(tempDescription)
            tempDescription = ""
        case XMLElementName.Link:
            self.isTagFound[XMLElementName.Link] = false
        case XMLElementName.PubDate:
            self.isTagFound[XMLElementName.PubDate] = false
        default:
            break
        }
    }
    
    /* End parsing document */
    func parserDidEndDocument(parser: NSXMLParser) {
        delegate?.parsingWasFinished()
    }
    
    // If any error detected while parsing.
    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError) {
        print(parseError.description)
    }
    
    func parser(parser: NSXMLParser, validationErrorOccurred validationError: NSError) {
        print(validationError.description)
    }
}
