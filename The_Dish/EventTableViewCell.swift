//
//  EventTableViewCell.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/3/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    
    // Constants for this class
    private struct Constants {
        static let NotAvailable = "N/A"
        static let Food = "Food? "
        static let Yes = "Yes"
        static let No = "No"
        static let DefaultImage = "default_image.png"
    }
    
    @IBOutlet private weak var eventPhotoImageView: UIImageView!
    @IBOutlet private weak var eventTitleLabel: UILabel!
    @IBOutlet private weak var eventPeopleLabel: UILabel!
    @IBOutlet private weak var eventDateLabel: UILabel!
    @IBOutlet private weak var eventLocationLabel: UILabel!
    @IBOutlet private weak var eventFoodLabel: UILabel!
    
    var event: Event? {
        didSet {
            updateUI()
        }
    }
    
    private func updateUI() {
        
        eventPhotoImageView.image = nil
        eventTitleLabel.text = nil
        eventDateLabel.text = nil
        eventLocationLabel.text = nil
        eventFoodLabel.text = nil
        
        if let event = self.event {
            eventTitleLabel.text = event.name
            eventDateLabel.text = event.date
            eventLocationLabel.text = event.location
            
            if event.isNumberOfPeopleAttendingAvailable {
                eventPeopleLabel.text = String(event.numberOfPeopleAttending)
            } else {
                eventPeopleLabel.text = Constants.NotAvailable
            }
            
            if let eventImage = event.picture {
                eventPhotoImageView.image = eventImage
            } else if let eventImageLink = event.imageLink {
                if let eventPhotoURL = NSURL(string: eventImageLink) {
                    NSURLSession.sharedSession().dataTaskWithURL(eventPhotoURL, completionHandler: { (data,response, error) in
                        if error != nil {
                            print(error)
                            return
                        }
                        
                        if let imageData = data {
                            if let image = UIImage(data: imageData) {
                                dispatch_async(dispatch_get_main_queue()) {
                                    self.eventPhotoImageView.image = image
                                }
                            }
                        }
                    }).resume()
                } else {
                    eventPhotoImageView.image = UIImage(named: Constants.DefaultImage)
                }     
            } else {
                eventPhotoImageView.image = UIImage(named: Constants.DefaultImage)
            }
            
            if event.isFoodInfoAvailable {
                if event.food {
                    eventFoodLabel.text = Constants.Food + Constants.Yes
                } else {
                    eventFoodLabel.text = Constants.Food + Constants.No
                }
            } else {
                eventFoodLabel.text = Constants.Food + Constants.NotAvailable
            }
        }
    }
}
