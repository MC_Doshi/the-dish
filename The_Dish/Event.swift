//
//  Event.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/3/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

import UIKit
import GoogleMaps

class Event {
    
    var name: String
    var startDate: NSDate?
    var endDate: NSDate?
    var date: String
    var location: String
    var googleMapsLocation: GMSPlace?
    var description: String
    var imageLink: String?
    var picture: UIImage?
    var isFoodInfoAvailable: Bool
    var food: Bool
    var isNumberOfPeopleAttendingAvailable: Bool
    var numberOfPeopleAttending: Int
    
    init(){
        self.name = ""
        self.date = ""
        self.location = ""
        self.description = ""
        //self.imageLink = ""
        self.isFoodInfoAvailable = false
        self.food = false
        self.isNumberOfPeopleAttendingAvailable = false
        self.numberOfPeopleAttending = 0
    }
}