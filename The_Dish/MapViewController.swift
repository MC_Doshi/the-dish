//
//  MapViewController.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/5/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {

    @IBOutlet private weak var mapView: GMSMapView!
    
    let locationManager = CLLocationManager()
    
    var events = [Event]()
    private var mapTasks = MapTasks()
    private var missingLocations = false
    private var eventDetails = Event()
    
    private struct Storyboard {
        static let EventSegue = "EventSegue"
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.clear()
        mapView.delegate = self
        
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        
        mapView.myLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    override func viewDidAppear(animated: Bool) {
        setUpLocationMarkers()
    }
    
    override func viewWillDisappear(animated: Bool) {
        if let destinationVC = self.tabBarController?.viewControllers?[0] as? UINavigationController {
            if let eventTableViewControllerTab = destinationVC.visibleViewController as? EventsTableViewController {
                eventTableViewControllerTab.events = events
            }
        }
    }
    
    // Adds a marker to the map
    private func setUpLocationMarker(event: Event, coordinate: CLLocationCoordinate2D) {
        let marker = GMSMarker()
        marker.position = coordinate
        marker.title = event.name
        marker.userData = event
        marker.map = self.mapView
    }
    
    private func setUpLocationMarkers() {
        for event in events {
            
            if let googleMapsLocation = event.googleMapsLocation {
                setUpLocationMarker(event, coordinate: googleMapsLocation.coordinate)
            } else {
                let address = event.location
                
                self.mapTasks.geocodeAddress(address, withCompletionHandler: { (status, success) -> Void in
                    if !success {
                        if status == "ZERO_RESULTS" {
                            self.missingLocations = true
                        }
                    } else {
                        let coordinate = CLLocationCoordinate2D(latitude: self.mapTasks.fetchedAddressLatitude, longitude: self.mapTasks.fetchedAddressLongitude)
                        
                        self.setUpLocationMarker(event, coordinate: coordinate)
                    }
                })
            }
        }
        
        if missingLocations {
            showAlertWithMessage("Some locations could not be found.")
        }
    }
    
    private func showAlertWithMessage(message: String) {
        let alertController = UIAlertController(title: "Missing Locations", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let closeAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel) { (alertAction) -> Void in
            
        }
        alertController.addAction(closeAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    // Marke: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Storyboard.EventSegue {
            if let eventVC = segue.destinationViewController as? EventViewController {
                eventVC.event = eventDetails
            }
        }
    }
}



// MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    
    func mapView(mapView: GMSMapView, didTapInfoWindowOfMarker marker: GMSMarker) {
        if let data = marker.userData as? Event {
            eventDetails = data
            self.performSegueWithIdentifier("EventSegue", sender: self)
        }
    }
}

// MARK: - CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
}
