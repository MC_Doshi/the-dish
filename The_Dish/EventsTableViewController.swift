//
//  EventsTableViewController.swift
//  The_Dish
//
//  Created by Michael-Anthony Doshi on 6/2/16.
//  Copyright © 2016 Michael Doshi. All rights reserved.
//

// Resources 
// https://www.youtube.com/watch?v=BIgqHLTZ_a4
// http://stackoverflow.com/questions/16663618/async-image-loading-from-url-inside-a-uitableview-cell-image-changes-to-wrong

import UIKit

class EventsTableViewController: UITableViewController, XMLParserDelegate {
    
    // Constants for Storyboard
    private struct Storyboard {
        static let EventCellIdentifier = "Event"
        static let EventSegue = "EventSegue"
        static let AddFiltersSegue = "AddFiltersSegue"
    }
    
    // Constants for this class
    private struct Constants {
        static let CacheDiskPath = "DefaultCacheDiskPath"
        static let EventURL = "http://events.stanford.edu/xml/rss.xml"
        static let DateFormat = "EEEE, MMMM dd, yyyy hh:mm a"
        static let Ongoing = "Ongoing"
    }
    
    private var xmlParser: XMLParser!
    
    var events = [Event]() {
        didSet {
            if isFilteredByTime {
                events.sortInPlace({ $0.startDate!.compare($1.startDate!) == NSComparisonResult.OrderedAscending })
            }
            if isFilteredByLocation {
                //events.sortInPlace({  })
            }
            if isFilteredByAttendance {
                events.sortInPlace({ $0.numberOfPeopleAttending > $1.numberOfPeopleAttending })
            }
            if isFilteredByFood {
                events.sortInPlace({ $0.food && !$1.food })
            }
            tableView.reloadData()
            
            if let destinationVC = self.tabBarController?.viewControllers?[1] as? UINavigationController {
                if let mapViewControllerTab = destinationVC.visibleViewController as? MapViewController {
                    mapViewControllerTab.events = events
                }
            }
        }
    }
    
    private var isFilteredByTime = true
    private var isFilteredByLocation = false
    private var isFilteredByAttendance = false
    private var isFilteredByFood = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let memoryCapacity = 500 * 1024 * 1024
        let diskCapacity = 500 * 1024 * 1024
        let urlCache = NSURLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: Constants.CacheDiskPath)
        NSURLCache.setSharedURLCache(urlCache)
        
        fetchEvents()
    }
    
    /* Uses an XMLParser to fetch events from the RSS feed of a given URL */
    private func fetchEvents() {
        if let url = NSURL(string: Constants.EventURL) {
            xmlParser = XMLParser()
            xmlParser.delegate = self
            xmlParser.startParsingWithContentsOfURL(url)
        } else {
            self.refreshControl?.endRefreshing()
        }
    }
    
    func parsingWasFinished() {
        self.refreshControl?.endRefreshing()
        extractEventInfo(xmlParser.rssRecordList)
    }
    
    /* Extracts info from RSSRecords and stores it as an Event */
    private func extractEventInfo(rssRecordList: [RSSRecord]) {
        for record in rssRecordList {
            
            // Only store events that have dates that match the format
            if record.date.rangeOfString(Constants.Ongoing) == nil {
                let event = Event()
                event.name = record.title
                event.date = record.date
            
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = Constants.DateFormat
                if let startDate = dateFormatter.dateFromString(record.date) {
                    event.startDate = startDate
                }
            
                event.location = record.location
                event.description = record.description
                event.imageLink = record.imageLink
                events.append(event)
            }
        }
    }
    
    @IBAction func refresh(sender: UIRefreshControl) {
        fetchEvents()
    }

    private func filterEvents() {
        if isFilteredByTime {
            events.sortInPlace({ $0.startDate!.compare($1.startDate!) == NSComparisonResult.OrderedAscending })
        }
        if isFilteredByLocation {
            //events.sortInPlace({  })
        }
        if isFilteredByAttendance {
            events.sortInPlace({ $0.numberOfPeopleAttending > $1.numberOfPeopleAttending })
        }
        if isFilteredByFood {
            events.sortInPlace({ $0.food && !$1.food })
        }
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Storyboard.EventCellIdentifier, forIndexPath: indexPath)
        let event = events[indexPath.row]
        if let eventCell = cell as? EventTableViewCell {
            eventCell.event = event
        }
        return cell
    }
    

    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Storyboard.EventSegue {
            if let eventVC = segue.destinationViewController as? EventViewController {
                if let eventCell = sender as? EventTableViewCell {
                    eventVC.title = eventCell.event?.name
                    eventVC.event = eventCell.event
                }
            }
        } else if segue.identifier == Storyboard.AddFiltersSegue {
            
            var destinationVC = segue.destinationViewController
            if let navCon = destinationVC as? UINavigationController {
                destinationVC = navCon.visibleViewController ?? destinationVC
            }
            if let filterEventsTVC = destinationVC as? FilterEventsTableViewController {
                filterEventsTVC.filteredByTime = isFilteredByTime
                filterEventsTVC.filteredByLocation = isFilteredByLocation
                filterEventsTVC.filteredByAttendance = isFilteredByAttendance
                filterEventsTVC.filteredByFood = isFilteredByFood
            }
        }
    }
    
    @IBAction func cancelToEventsTableViewController(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func saveNewEvent(segue: UIStoryboardSegue) {
        if let addEventTableViewController = segue.sourceViewController as? AddEventTableViewController {
            if let event = addEventTableViewController.event {
                events.append(event)
            }
        }
    }
    
    @IBAction func applyNewFilters(segue: UIStoryboardSegue) {
        if let filterEventsTableViewController = segue.sourceViewController as? FilterEventsTableViewController {
            isFilteredByTime = filterEventsTableViewController.filteredByTime
            isFilteredByLocation = filterEventsTableViewController.filteredByLocation
            isFilteredByAttendance = filterEventsTableViewController.filteredByAttendance
            isFilteredByFood = filterEventsTableViewController.filteredByFood
            filterEvents()
        }
    }
}
